﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semana2
{
    class Matriz
    {
        private string[] workers;
        private int[,] salario;
        private int[] aux;

        public void Cargar()
        {
            workers = new String[4];
            salario = new int[4, 3];
            for (int f = 0; f < workers.Length; f++)
            {
                Console.Write("Ingrese el nombre del operario " + (f + 1) + ": ");
                workers[f] = Console.ReadLine();
                for (int c = 0; c < salario.GetLength(1); c++)
                {
                    Console.Write("Ingrese sueldo " + (c + 1) + ": ");
                    string linea;
                    linea = Console.ReadLine();
                    salario[f, c] = int.Parse(linea);
                }
            }
        }

        public void CalcularSumaSueldos()
        {
            aux = new int[4];
            for (int f = 0; f < salario.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < salario.GetLength(1); c++)
                {
                    suma = suma + salario[f, c];
                }
                aux[f] = suma;
            }
        }

        public void ImprimirTotalPagado()
        {
            Console.WriteLine("Total de sueldos pagados por Operario.");
            for (int f = 0; f < aux.Length; f++)
            {
                Console.WriteLine(workers[f] + " - " + aux[f]);
            }
        }

        public void EmpleadoMayorSueldo()
        {
            int may = aux[0];
            string nom = workers[0];
            for (int f = 0; f < aux.Length; f++)
            {
                if (aux[f] > may)
                {
                    may = aux[f];
                    nom = workers[f];
                }
            }
            Console.WriteLine("El operario con mayor sueldo es " + nom + " que tiene un sueldo de " + may);
        }

        static void Main(string[] args)
        {
            Matriz ma = new Matriz();
            ma.Cargar();
            ma.CalcularSumaSueldos();
            ma.ImprimirTotalPagado();
            ma.EmpleadoMayorSueldo();
            Console.ReadKey();
        }
    }
}
